import React from 'react'
import { Link } from "react-router-dom"
import {Table, Button} from "reactstrap"

const AvailableTasksTable = () => {

    const handleApproveTask = () =>{}

    const handleRejectedTask = () => {}

    const handleContactManager = () =>{
        console.log("Contact Manager")
    }

    const tasks = [
      {
        id: "R182007A",
        status:"Assigned",
        name: "Underwrite",
        assignee: "Health Underwriter",
      }
    ];
  
    return (
        <Table striped bordered hover>
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
  
          { (tasks.length > 0) ? tasks.map( (tk, index) => {
           return (
            <tr key={ index }>
              <td>{ tk.id }</td>
              <td>{ tk.name }</td>
              <td>{ tk.status}</td>
              <td>
              {tk.id ?
                <Link to="/task/underwriter/details" className="btn btn-success">Details</Link>: null
                }
                
              { tk.id ?
                <Link  to="/task/underwriter/complete"className="btn btn-danger ml-2">Complete</Link>: null
              }
              { tk.id ?
                 <Button className="btn btn-warning ml-2" onClick={()=>handleContactManager()}>Contact Manager</Button>: null
              }
              </td>
            </tr>
          )
         }) : <tr><td colSpan="5"><h1>No Tasks Yet</h1></td></tr> }
        </tbody>
      </Table>
    )
}

export default AvailableTasksTable
