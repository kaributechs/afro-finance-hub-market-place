import React from "react";
import { Card, CardBody, CardTitle, CardFooter} from "reactstrap";


const CardsButton = ({cardTitle, handleClick}) => {
  return (
    <Card>
      <CardBody>
        <CardTitle tag="h4">{cardTitle}</CardTitle>
      </CardBody>
        <CardFooter>
            {handleClick}
        </CardFooter>
      
    </Card>
  );
};

export default CardsButton;
