import { useTranslation } from "react-i18next";
const useTrans = () => {
    const [t, i18n] = useTranslation();

    const handleClick = (lang) => {
      i18n.changeLanguage(lang);
    };

    return[t, handleClick]

}

export default useTrans
