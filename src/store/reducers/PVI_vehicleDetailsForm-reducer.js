const {ADD_VEHICLEDETAILS_DATA} = require("../types/personalInsuranceApp-types")

const initialVehicleState = {
    formData:{
        vehicleRegistrationNumber:"",
        clientLicenseNumber:"",
        dateWhenLicenseWasObtained:"",
        vehicleMake:"",
        vehicleModel:"",
        vehicleYear:"",
        vehicleMileage:"",
        vehicleWorth:"",
        numberOfSeats:"",
        vehicleHasAlarmSystem:"",
        residentHasNightParking:"",
        isVehicleUsedByOthers:""
    }
}

const vehicleDetailsFormReducer = (state = initialVehicleState, action) =>{
    switch(action.type){
        case ADD_VEHICLEDETAILS_DATA: return {
            ... state,
            formdata: action.payload,
            
        }
        default: return state
    }
}
export default vehicleDetailsFormReducer