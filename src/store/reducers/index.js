import {layoutReducer} from "./layout-reducer";
import claimsReducer from "./claims-reducer";
// PVI= Personal Vehicle Insurance
import personalDetailsReducer from "./PVI_personalDetailsForm-reducer";
import contactDetailsFormReducer from "./PVI_contactDetailsForm-reducer"
import vehicleDetailsFormReducer from "./PVI_vehicleDetailsForm-reducer"
import secondaryDriverDetailsFormReducer from "./PVI_secondaryDriverDetails-reducer"
import insuranceQuesFormReducer from "./PVI_insuranceQuestionsForm"
import insuredFormReducer from "./insuredFormReducer";
import vehicleFormReducer from "./vehicleFormReducer";
import driversFormReducer from "./driversFormReducer";
import questionsFormReducer from "./questionsFormReducer";
import {combineReducers} from 'redux';
import {audioReducer} from "./audioReducer";


export default combineReducers({
    layout: layoutReducer,
    claims: claimsReducer,
    applicant: personalDetailsReducer,
    contact: contactDetailsFormReducer,
    secondaryDriver: secondaryDriverDetailsFormReducer,
    insuranceQuestions: insuranceQuesFormReducer,
    insured: insuredFormReducer,
    vehicle: vehicleFormReducer,
    drivers: driversFormReducer,
    questions: questionsFormReducer,
    audio: audioReducer
});
