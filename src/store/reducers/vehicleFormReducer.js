const { ADD_VEHICLEFORM_DATA } = require("../types/car-claim-form-types")

const initialVehicleState = {
    formdata: {
        VehicleDetails: {
            vehicleMake:"",
            vehicleModel:"",
            vehicleModelYear:"",
            vehicleRegistrationNumber:"",
            vehicleMilage:"",
            vehicleUsage:""
        }
    }

}

const vehicleFormReducer = (state = initialVehicleState, action) => {
    switch (action.type) {
        case ADD_VEHICLEFORM_DATA: return {
            ...state,
            formdata: action.payload,

        }
        default: return state
    }
}
export default vehicleFormReducer

