const {ADD_SECONDARYDRIVERDETAILS_DATA} = require("../types/personalInsuranceApp-types")

const initialSecondaryDriverState = {
    formData:{
        dependentLicenseNumber:"",
        dependentDateWhenLicenseWasObtained:"",
        dependentNationalId:"",
        relationshipToBeneficiary:"",
        dependentFirstName:"",
        dependentLastName:"",
        dependentEmailAddress:"",
        dependentDateOfBirth:"",
        dependentPhoneNumber:"",
        dependentGender:""
    }
}

const secondaryDriverDetailsFormReducer = (state = initialSecondaryDriverState, action) =>{
    switch(action.type){
        case ADD_SECONDARYDRIVERDETAILS_DATA: return {
            ... state,
            formdata: action.payload,
            
        }
        default: return state
    }
}
export default secondaryDriverDetailsFormReducer