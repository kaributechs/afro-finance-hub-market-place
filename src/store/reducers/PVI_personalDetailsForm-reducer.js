const {ADD_PERSONALDETAILS_DATA} = require("../types/personalInsuranceApp-types")

const initialApplicantState = {
    formData:{
        salutation:"",
        nationalId:"",
        clientFirstName:"",
        clientLastName:"",
        dateOfBirth:"",
        clientGender:"",
        maritalStatus:"",
    }
}

const personalDetailsFormReducer = (state = initialApplicantState, action) =>{
    switch(action.type){
        case ADD_PERSONALDETAILS_DATA: return {
            ... state,
            formdata: action.payload,
            
        }
        default: return state
    }
}
export default personalDetailsFormReducer