const { AUDIO_SELECT, AUDIO_PLAY, AUDIO_PAUSE } = require("../types/audio-types")


const initialState = {
    audioTrack: null, 
    isPlaying: false,
};

export const audioReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUDIO_SELECT: 
    return {
        ...state,
        audioTrack: action.payload,
    }
    case AUDIO_PLAY:
    return {
        ...state,
        isPlaying: action.payload,
    }
    case AUDIO_PAUSE:
    return {
        ...state,
        isPlaying: action.payload,
    }
    default: return state
  }

  
}
