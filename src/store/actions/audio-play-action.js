import { AUDIO_PLAY } from "../types/audio-types"

// sets current audio    
export const playAudioTrack = (formdata) => (dispatch) => {
    dispatch({
        type: AUDIO_PLAY,
        payload: formdata,
        
      });
}
