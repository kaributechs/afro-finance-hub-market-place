import { ADD_PERSONALDETAILS_DATA } from "../types/personalInsuranceApp-types"

export const personalDetailsTask = (formdata) => (dispatch) => {

        dispatch({
          type: ADD_PERSONALDETAILS_DATA,
          payload: formdata,
          
        });

  
  }