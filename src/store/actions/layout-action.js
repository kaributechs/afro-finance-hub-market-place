import {SIDEBAR_SHOW, SIDEBAR_UNFOLDABLE} from "../types";

export const layoutSideBarShow = (sidebarShow) => (dispatch) => {

      dispatch({
        type: SIDEBAR_SHOW,
        payload: sidebarShow,
      });

};

export const layoutSideBarUnfoldable = (sidebarShow) => (dispatch) => {

  dispatch({
    type: SIDEBAR_UNFOLDABLE,
    payload: sidebarShow,
  });

};
