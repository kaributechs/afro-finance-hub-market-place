import {ClaimsClientService} from "../../services";
import {CLAIM_Available_Tasks} from "../types";

export const fetchClaimsAvailableTasks = () => (dispatch) => {

  const service = new ClaimsClientService();

  return service.fetchAvailableTasks()
    .then(result=>{
      dispatch({
        type: CLAIM_Available_Tasks,
        payload: result,
      });
      return result;
    })

}

