import React from "react";
import {Row, Col} from 'reactstrap'
import useTrans from "../../hooks/useTrans";
import { Link } from "react-router-dom"
import ScreensCards from "../../components/cards/ScreensCards";
import { useKeycloak } from '@react-keycloak/web'

const ClaimsScreen = () => {

  // eslint-disable-next-line
  const [t, handleClick] = useTrans();
   // eslint-disable-next-line
  const { keycloak, initialized } = useKeycloak()

  const appManager  = keycloak.hasRealmRole('app-manager')

  const appCustomer = keycloak.hasRealmRole('app-customer')

  const appConsultant  = keycloak.hasRealmRole('app-consultant')
  
  return (
    <div className="animated fadeIn">

      <Link to="/dashboard" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
      <Row>

      {appCustomer ?
        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("LodgeClaim")}
            footerText={t("Claim")}
            cardRoute="claims/type"
          />
        </Col> : null}

    {appCustomer ?
        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("DamageAssessment")}
            footerText={t("Damage")}
            cardRoute="damage"
          />
        </Col>

        : null}

      {appCustomer ? 
        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("TrackClaim")}
            footerText={t("Track")}
            cardRoute="track"
          />
        </Col> : null}

        {appCustomer ? 
        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("CancelClaim")}
            footerText={t("Cancel")}
            cardRoute="cancel"
          />
        </Col>
        : null}

     {appCustomer ? 
        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("SubmitDocumentation")}
            footerText={t("Submit")}
            cardRoute="submit"
          />
        </Col>
     : null}

      {appManager ?
        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("Administer")}
            footerText={t("Admin")}
            cardRoute="claims/adminster"
          />
        </Col>

      : null}

   {appConsultant ? 
    <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={"Assessor Tasks"}
            footerText={"Tasks"}
            cardRoute="claims/consultant-tasks"
          />
        </Col> 
    : null}

      
      </Row>
    </div>
  );
};

export default ClaimsScreen;
