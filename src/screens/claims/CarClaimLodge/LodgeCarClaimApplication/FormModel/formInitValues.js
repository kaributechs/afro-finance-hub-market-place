import lodgeCarClaimModel from './lodgeCarClaimModel';

const {
  formField: {
    screenId,
  InsuredDetails:{
   insuredId,
   insuredSalutation,
   insuredFirstName,
   insuredLastName,
   insuredPhoneNumber,
   insuredEmail,
   insuredResidentialStreetAddress,
   insuredSurbub,
   insuredResidentialCity,
   insuredRegion,
   insuredCountry,
   insuredZipCode,
   insuredWorkingStreetAddress,
   insuredWorkingSurbub,
   insuredWorkingCity,
   insuredWorkingRegion,
   insuredWorkingCountry,
   insuredWorkingZipCode}, 

  VehicleDetails:{
   vehicleMake,
   vehicleModel,
   vehicleModelYear,
   vehicleRegistrationNumber,
   vehicleMilage,
   vehicleUsage
    },
    

  DriversDetails:{
    driversFirstName,
    driversLastName,
    driversEmail,
    driversPhoneNumber,
    driversResidentialStreetAddress,
    driversSurbub,
    driversResidentialCity,
    driversRegion,
    driversCountry,
    driversZipCode,
    driversWorkingStreetAddress,
    driversWorkingSurbub,
    driversWorkingCity,
    driversWorkingRegion,
    driversWorkingCountry,
    driversWorkingZipCode,
    driversLicenseNumber,
    dateOfLicenseIssue
    },

    FinalPageDetails:{
    driversCondition,
    accidentHistory,
    accidentHistoryDetails,
    driversConditionDetails,
    vehiclePurpose, 
    natureOfInjuries,
    accidentDescription,

    },
  }
  
} = lodgeCarClaimModel;

export default {
  formField:{
    [screenId.name]:"",
    InsuredDetails:{
      [insuredId.name]: "",
      [insuredSalutation.name]: "",
      [insuredFirstName.name]: "",
      [insuredLastName.name]:"",
      [insuredPhoneNumber.name]:"",
      [insuredEmail.name]:"",
      [insuredResidentialStreetAddress.name]:"",
      [insuredSurbub.name]:"",
      [insuredResidentialCity.name]:"",
      [insuredRegion.name]:"",
      [insuredCountry.name]:"",
      [insuredZipCode.name]:"",
      [insuredWorkingStreetAddress.name]:"",
      [insuredWorkingSurbub.name]:"",
      [insuredWorkingCity.name]:"",
      [insuredWorkingRegion.name]:"",
      [insuredWorkingCountry.name]:"",
      [insuredWorkingZipCode.name]:""},
  
    VehicleDetails:{
     
      [vehicleMake.name]:"",
      [vehicleModel.name]:"",
      [vehicleModelYear.name]:"",
      [vehicleRegistrationNumber.name]:"",
      [vehicleMilage.name]:"",
      [vehicleUsage.name]:""},
  
    DriversDetails:{
      [  driversFirstName.name]:"",
      [  driversLastName.name]:"",
      [  driversEmail.name]:"",
      [  driversPhoneNumber.name]:"",
      [  driversResidentialStreetAddress.name]:"",
      [driversSurbub.name]:"",
      [  driversResidentialCity.name]:"",
      [  driversRegion.name]:"",
      [  driversCountry.name]:"",
      [  driversZipCode.name]:"",
      [  driversWorkingStreetAddress.name]:"",
      [driversWorkingSurbub.name]:"",
      [  driversWorkingCity.name]:"",
      [  driversWorkingRegion.name]:"",
      [  driversWorkingCountry.name]:"",
      [  driversWorkingZipCode.name]:"",
      [  driversLicenseNumber.name]:"",
      [  dateOfLicenseIssue.name]:""
    },
  
    FinalPageDetails:{
      
      [driversCondition.name]:"",
      [accidentHistory.name]:"",
      [accidentHistoryDetails.name]:"",
      [driversConditionDetails.name]:"",
      [vehiclePurpose.name]:"",
      [natureOfInjuries.name]:"",
      [accidentDescription.name]:""
      },
  }
  


} ;

