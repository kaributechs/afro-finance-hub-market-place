import React, { Component } from "react";

class ToggleBox2 extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			opened: false,
		};
		this.toggleBox = this.toggleBox.bind(this);
	}
  
	toggleBox() {
		const { opened } = this.state;
		this.setState({
			opened: !opened,
		});
	}
  
	render() {
		var { title, children } = this.props;
		const { opened } = this.state;

		if (opened){
			title =<span style={{color:'#4d79ff'}}>Minimize recording panel</span>;
		}else{
			title =<span style={{fontSize:15,textDecorationLine: 'underline',color:'brown'}}>Open recording panel.</span>
            
		}
		
		return (
			<div className="box">
				<div className="boxTitle" onClick={this.toggleBox}>
					{title}

				</div>
				{opened && (					
					<div class="boxContent">
						{children}
					</div>
				)}
			</div>
		);
	}
}

export default ToggleBox2;

