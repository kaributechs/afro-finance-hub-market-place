import React from 'react';
import { ReactMic } from 'react-mic';
import { Component } from 'react';
import { Link } from "react-router-dom"

 class Recorder extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      record: false
    }
  }
 
  startRecording = () => {
    this.setState({ record: true });
  }
 
  stopRecording = () => {
    this.setState({ record: false });
  }
 
  onData(recordedBlob) {
    console.log('chunk of real-time data is: ', recordedBlob);
  }
 
  onStop(recordedBlob) {
    console.log('recordedBlob is: ', recordedBlob);
  }
 
  render() {
    return (
      <div>
        <br></br>
        <span style={{fontSize:'10'}}>PRESS START TO BEGIN RECORDING AND STOP WHEN DONE</span>
        <br></br>
        <button onClick={this.startRecording} type="button">Start</button>
        <span style={{paddingRight:'30px'}}></span>
        <button onClick={this.stopRecording} type="button">Stop</button>
        <span style={{paddingRight:'30px'}}></span>
        <ReactMic
          record={this.state.record}
          className="sound-wave"
          onStop={this.onStop}
          onData={this.onData}
          strokeColor="white"
          backgroundColor="#8ec2de" />
        
       
      </div>
    );
  }
}

export default Recorder
