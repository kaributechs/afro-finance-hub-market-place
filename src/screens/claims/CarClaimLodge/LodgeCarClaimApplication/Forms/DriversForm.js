import React, { useState } from 'react';
import { Col, Input, FormGroup, Label } from 'reactstrap';
import { CountryDropdown, RegionDropdown } from 'react-country-region-selector';
import { useFormikContext } from 'formik';
import { InputField } from "../../FormFields";
import { CFormLabel } from "@coreui/react";

export const DriversForm = (props) => {

  const {
    formField: {
      screenId,
      DriversDetails: {
        driversLicenseNumber,
        dateOfLicenseIssue,
        driversFirstName,
        driversLastName,
        driversRegion,
        driversEmail,
        driversPhoneNumber,
        driversResidentialStreetAddress,
        driversSurbub,
        driversResidentialCity,
        driversCountry,
        driversZipCode,
        driversWorkingStreetAddress,
        driversWorkingSurbub,
        driversWorkingCity,
        driversWorkingRegion,
        driversWorkingCountry,
        driversWorkingZipCode,

      }


    }
  } = props;

  //TODO: fix form field for country and region
  const { values: formValues } = useFormikContext();

  const [countryDll, setCountryDll] = useState(formValues.driversCountry)
  const [regionDll, setRegionDll] = useState(formValues.driversRegion)
  const [workCountryDll, setWorkCountryDll] = useState(formValues.driversWorkingCountry)
  const [workRegionDll, setWorkRegionDll] = useState(formValues.driversWorkingRegion)


  const handleCountryChange = (country) => {
    formValues.driversCountry = country;
    setCountryDll(country);
  }

  const handleRegionChange = (region) => {
    formValues.driversRegion = region;
    setRegionDll(region);
  }

  const handleWorkCountryChange = (workCountry) => {
    formValues.driversWorkingCountry = workCountry;
    setWorkCountryDll(workCountry);
  }

  const handleWorkRegionChange = (workRegion) => {
    formValues.workRegion = workRegion;
    setWorkRegionDll(workRegion);
  }

 screenId.name = 'Drivers_Form_Page'
  return (

    <div className="animated fadeIn">

      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <InputField type="text"
            name={driversFirstName.name}
            id={driversFirstName.name}
            label={<Label htmlFor='driversFirstName'><span style={{ color: "red" }}>*</span>First Name</Label>}
            autoComplete="FirstName"
            placeholder="First Name" />

        </Col>

        <Col xs="12" sm="6" lg="6">
          <InputField type="text"
            name={driversLastName.name}
            id={driversLastName.name}
            label={<Label htmlFor='driversLastName'><span style={{ color: "red" }}>*</span>Last Name</Label>}
            autoComplete='Last Name'
            placeholder="Last Name" />

        </Col>
      </FormGroup>


      <FormGroup row className="my-0">
        <span style={{ paddingTop: '15px' }}></span>
        <Col xs="12" sm="6" lg="6">
          <InputField type="email"
            name={driversEmail.name}
            id={driversEmail.name}
            label={<Label htmlFor={driversEmail.name}><span style={{ color: "red" }}>*</span>Email</Label>}
            autoComplete="email"
            placeholder="Email Address" />

        </Col>

        <Col xs="12" sm="6" lg="6">

          <InputField type="text"
            name={driversPhoneNumber.name}
            id={driversPhoneNumber.name}
            label={<Label htmlFor='driversPhoneNumber'><span style={{ color: "red" }}>*</span>Phone Number</Label>}
            autoComplete='Phone Number'
            placeholder="Phone Number" />

        </Col>
      </FormGroup>

      <hr />

      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <strong>Residential Address</strong>
          </FormGroup>
        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <strong>Work Address</strong>
          </FormGroup>
        </Col>
      </FormGroup>


      <FormGroup row className="my-0">
        <span style={{ paddingTop: '15px' }}></span>
        <Col xs="12" sm="6" lg="6">
          <InputField type="textarea"
            rows={2}
            name={driversResidentialStreetAddress.name}
            id={driversResidentialStreetAddress.name}
            autoComplete='Address'
            label={<Label htmlFor={driversResidentialStreetAddress.name}><span style={{ color: "red" }}>*</span>Street Address</Label>}
            placeholder="Street Address" />

        </Col>

        <Col xs="12" sm="6" lg="6">
          <InputField type="textarea"
            rows={2}
            name={driversWorkingStreetAddress.name}
            id={driversWorkingStreetAddress.name}
            autoComplete='Address'
            label={<Label htmlFor={driversWorkingStreetAddress.name}><span style={{ color: "red" }}>*</span>Working Street Address</Label>}
            placeholder="Working Street Address" />

        </Col>
      </FormGroup>

<FormGroup row className="my-0">
        <span style={{ paddingTop: '15px' }}></span>
        <Col xs="12" sm="6" lg="6">
          <InputField type="textarea"
            rows={2}
            name={driversSurbub.name}
            id={driversSurbub.name}
            autoComplete='Surbub'
            label={<Label htmlFor={driversSurbub.name}><span style={{ color: "red" }}>*</span>Surbub</Label>}
            placeholder="Surbub" />

        </Col>

        <Col xs="12" sm="6" lg="6">
          <InputField type="textarea"
            rows={2}
            name={driversWorkingSurbub.name}
            id={driversWorkingSurbub.name}
            autoComplete='Address'
            label={<Label htmlFor={driversWorkingSurbub.name}><span style={{ color: "red" }}>*</span>Working Surbub</Label>}
            placeholder="Working Surbub" />

        </Col>
      </FormGroup>
      <FormGroup row className="my-0">
        <span style={{ paddingTop: '15px' }}></span>
        <Col xs="12" sm="6" lg="6">
          <InputField type="text"
            name={driversResidentialCity.name}
            id={driversResidentialCity.name}
            
            label={<Label htmlFor={driversResidentialCity.name}><span style={{ color: "red" }}>*</span>City</Label>}
            autoComplete='City'
            placeholder='City'
          />


        </Col>

        <Col xs="12" sm="6" lg="6">
          <InputField type="text"
            name={driversWorkingCity.name}
            id={driversWorkingCity.name}
            label={<Label htmlFor='driversWorkingCity'><span style={{ color: "red" }}>*</span>Working City</Label>}
            autoComplete='Working City'
            placeholder='Working City'
          />

        </Col>
      </FormGroup>


      <FormGroup row className="my-0">
        <span style={{ paddingTop: '15px' }}></span>
        <Col xs="12" sm="6" lg="6">

          <FormGroup>
            <CFormLabel for='driversCountry'><span style={{ color: "red" }}>*</span>Country</CFormLabel>
            <div style={{ borderColor: 'green' }}>
              <CountryDropdown
                name={driversCountry.name}
                className=" form-control mb-3"
                id={driversCountry.name}
                value={countryDll}
                onChange={handleCountryChange} />
            </div>
          </FormGroup>

        </Col>

        <Col xs="12" sm="6" lg="6">

          <FormGroup>
            <CFormLabel for='driversWorkingCountry'><span style={{ color: "red" }}>*</span>Working Country</CFormLabel>
            <div style={{ borderColor: 'green' }}>
              <CountryDropdown
                className=" form-control mb-3"
                name={driversWorkingCountry.name}
                id={driversWorkingCountry.name}
                value={workCountryDll}
                onChange={handleWorkCountryChange} />
            </div>
          </FormGroup>


        </Col>
      </FormGroup>


      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <CFormLabel for='driversRegion'><span style={{ color: "red" }}>*</span>Region</CFormLabel>
            <RegionDropdown
              country={formValues.driversCountry}
              className=" form-control mb-3"
              name={driversRegion.name}
              id={driversRegion.name}
              value={regionDll}
              onChange={handleRegionChange} />
          </FormGroup>



        </Col>

        <Col xs="12" sm="6" lg="6">

          <FormGroup>
            <CFormLabel for='driversWorkingRegion'><span style={{ color: "red" }}>*</span>Working Region</CFormLabel>
            <RegionDropdown
              country={formValues.driversWorkingCountry}
              className=" form-control mb-3"
              name={driversWorkingRegion.name}
              id={driversWorkingRegion.name}
              value={workRegionDll}
              onChange={handleWorkRegionChange} />
          </FormGroup>


        </Col>
      </FormGroup>
      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <InputField type="number"
            name={driversZipCode.name}
            id={driversZipCode.name}
            autoComplete='Zipcode'
            label={<Label htmlFor={driversZipCode.name}><span style={{ color: "red" }}>*</span>Zip Code</Label>}
          />


        </Col>

        <Col xs="12" sm="6" lg="6">
          <InputField type="number"
            name={driversWorkingZipCode.name}
            id={driversWorkingZipCode.name}
            autoComplete='Working Zipcode'
            label={<Label htmlFor={driversWorkingZipCode.name}><span style={{ color: "red" }}>*</span>Working Zip Code</Label>}
          />

        </Col>
      </FormGroup>
      <FormGroup row className="my-0">
        <span style={{ paddingTop: '15px' }}></span>
        <Col xs="12" sm="6" lg="6">
          <InputField type="textarea"
            name={driversLicenseNumber.name}
            label={<Label htmlFor={driversLicenseNumber.name}><span style={{ color: "red" }}>*</span>License Number</Label>}
            id={driversLicenseNumber.name}
            placeholder="License Number" />

        </Col>

        <Col xs="12" sm="6" lg="6">
          <InputField type="date"
            name={dateOfLicenseIssue.name}
            label={<Label htmlFor={dateOfLicenseIssue.name}><span style={{ color: "red" }}>*</span>Date Of License Issue</Label>}
            id={dateOfLicenseIssue.name}
            placeholder="Date of license issue"
            autoComplete="new-dateOfIssue"
          />
        </Col>



      </FormGroup>


    </div>
  )
}
