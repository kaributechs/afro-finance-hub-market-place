import React, {useState, useEffect} from 'react'
import { Link } from "react-router-dom"
import { Container } from "reactstrap"
import axios from 'axios'
import AvailableTasksTable from '../../../components/tables/AvailableTasksTable'
const AvailableTasks = () => {

const [task, setTask] = useState([]);
  useEffect(() => {
    const fetchAvailableTasks = async () => {
      const {data} = await axios.get(`/api/v1/tasks/my-tasks`);
      setTask(data);
      console.log("**Available Tasks**", data);
    };
    fetchAvailableTasks();
  }, []);

  return (
    <div>
    <Link to="/claims/adminster" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
      <Container>
        <AvailableTasksTable tasks={task} />
      </Container>
    </div>
  );
};

export default AvailableTasks
