import React, {useState, useEffect} from 'react'
import axios from 'axios'
import {Link} from 'react-router-dom'
import ApproveTasksTable from "../../../components/tables/ApproveTasksTable"
import { useDispatch, useSelector } from 'react-redux';
import {fetchClaimsAvailableTasks} from "../../../store/actions";

const ApproveTasks = () => {

   const task = useSelector((state) => state.claims.tasks);
   const dispatch = useDispatch();

    useEffect(() => {
      dispatch(fetchClaimsAvailableTasks());
    },[])



    //Handle Approve Task
      const handleApproveTask = async(event) => {
        const res  = await axios.post(`/api/v1/task/${event.id}/complete`,{
          approved: true,
          autoAssess: true
        })
        dispatch(fetchClaimsAvailableTasks());
      }


      //Get More Details Task
      const handleMoreDetailsTask = async(event) =>{
        const res = await axios.post(`/api/v1/task/${event.id}/complete`,{
          getMoreDetails: true
        })
        dispatch(fetchClaimsAvailableTasks());
      }

      //Handle Reject Task
      const handleRejectedTask= async(event) =>{
        const res  = await axios.post(`/api/v1/task/${event.id}/complete` ,{
          approved: false
        })
        dispatch(fetchClaimsAvailableTasks());
      }


    return (
        <>
          <Link to="/claims/adminster" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
          <ApproveTasksTable
              tasks ={task}
              handleApproveTask = {handleApproveTask}
              handleRejectedTask = {handleRejectedTask}
              handleMoreDetails = {handleMoreDetailsTask}
          />
      </>
    )
}

export default ApproveTasks
