import React from "react";
import { Row, Col } from "reactstrap";
import ScreensCards from '../../components/cards/ScreensCards'
import { Link } from "react-router-dom"
import useTrans from "../../hooks/useTrans";

const DealersScreen = () => {
      // eslint-disable-next-line
  const [t, handleClick] = useTrans();

  return (
    <div>
    <React.Fragment>
      <Link to="/dashboard" className="mb-2"><span class="material-icons">keyboard_backspace</span></Link>
    <Row>
      <Col xs="12" sm="6" lg="3">
        <ScreensCards
          cardTitle={t("PanelBeaters")}
          footerText={t("PanelBeaters")}
          cardRoute="beaters"
        />
      </Col>
      </Row>
    </React.Fragment>
    </div>
  );
};

export default DealersScreen;
