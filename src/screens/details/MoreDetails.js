import React, { useState, useEffect } from "react";
import './Details.css'
import {
  Col,
  Row,
  Button,
  Card,
  CardBody,
  CardHeader,
  CardFooter,
  Table} from "reactstrap";
import { Link } from "react-router-dom";
import axios from "axios";

const MoreDetails = (props) => {
  const [task, setTask] = useState({});
  const { processInstanceId } = props.match.params;

  useEffect(() => {
    getTaskById();
  }, []);

  console.log("**My Task By Id**", task);
  const getTaskById = async () => {
    const res = await axios.post(
      `/api/v1/process/${processInstanceId}/variables`
    );
    setTask(res.data);
  };

  return (
    <div className="animated fadeIn">
      <Link to="/approve-tasks" className="mb-2">
        <span className="material-icons">keyboard_backspace</span>
      </Link>

      <Card>
        <CardHeader>
          <strong>Claim Details</strong>
        </CardHeader>
        <CardBody>
          <Table>
          <Row className="font__size">
            <Col xs="2"><span > User Email</span></Col>
            <Col x6="10"><span className="ml-3 mt-3">{task.clientEmail}</span></Col>
          </Row>

          <Row className="font__size"> <Col xs="2"><span>Client First Name</span></Col>
            <Col xs="10"><span className="ml-3 mt-3">{task.clientFirstName}</span></Col>
          </Row>

          <Row className="font__size">
            <Col xs="2"><span>Last Name</span></Col>
            <Col xs="10"><span className="ml-3 mt-3">{task.clientSurname}</span></Col>
          </Row>

          <Row className="font__size">
            <Col xs="2"><span>Car Type</span></Col>
            <Col xs="10"><span className="ml-3 mt-3">{task.carType}</span></Col>
          </Row>

          <Row className="font__size">
              <Col xs="2"><span>Description</span></Col>
              <Col xs="10"><span className="ml-3 mt-3">{task.description}</span></Col>
          </Row>

          <Row className="font__size">
            <Col xs="2"><span>Claim Status</span></Col>
            <Col xs="10"><span className="ml-3 mt-3">{task.claimStatus}</span> </Col>
          </Row>  

          </Table>
          

        </CardBody>
        <CardFooter>
          <Button className="btn btn-success ml-3 mt-3">Approve</Button>
          <Button className="btn btn-danger ml-3 mt-3">Decline</Button>
        </CardFooter>
      </Card>
    </div>
  );
};

export default MoreDetails;
