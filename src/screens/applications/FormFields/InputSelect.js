import React from 'react';
import {at} from 'lodash';
import {useField} from 'formik';
import {CFormLabel, CFormSelect} from "@coreui/react";
import {FormFeedback, FormGroup} from "reactstrap";


export const InputSelect = (props) => {

  const {name, label, data, value, text, ...rest} = props;
  const [field, meta] = useField(props);


  return (
    <FormGroup>
      <CFormLabel htmlFor={name}>{label}</CFormLabel>
      <CFormSelect className="mb-3"
                   invalid={(meta.submitCount || meta.touched) && meta.error && true}
                   valid={  meta.touched && !meta.error}
                   {...field}
                   {...rest} >
        {
          data.map((item, index) => (
            <option key={index} value={item[value]}>{item[text]}</option>
          ))}
        }
      </CFormSelect>
      <FormFeedback>{meta.error}</FormFeedback>
    </FormGroup>

  );
}
