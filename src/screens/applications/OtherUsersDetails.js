import React from 'react';
import {  Card, CardHeader, CardBody, Col, Form, FormFeedback, FormGroup, Label, Input} from 'reactstrap';
import {Link} from 'react-router-dom'
import { Formik } from 'formik';

import * as Yup from 'yup'


const validationSchema = function () {
  return Yup.object().shape({
    dependentNationalId: Yup.string()
    .matches(/^\(?([0-9]{2})\)?[-. ]?([0-9]{7})([A-Z]{1})[-. ]?([0-9]{2})$/, 'Please Enter A Valid ID Number')
    .min(14, `National ID has to be at least 14 characters`)
    .required('National ID is required'),
    dependentLicenseNumber: Yup.string()
    .required('License Number is Required!'),
    relationshipToBeneficiary: Yup.string()
    .required('Relationship to Beneficiary is Required!'),
    dependentFirstName: Yup.string()
    .min(2, `First Name has to be at least 2 characters`)
    .required('First Name is required'),
    dependentLastName: Yup.string()
    .min(2, `Last Name has to be at least 2 characters `)
    .required('Last Name is required'),
    dependentEmailAddress: Yup.string()
    .matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, 'Invalid email address!')
    .email('Invalid email address')
    .required('Email is required!'),
    dependentDateOfBirth: Yup.string()
    .required('Date of Birth Is Required'),
    dependentPhoneNumber: Yup.string()
    .matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
    .max(10, 'Phone number should be at least 10 characters')
    .required('Phone Number is required'),
    dependentGender: Yup.string()
    .required('Gender is required!'),

  })
}

const validate = (getValidationSchema) => {
  return (values) => {
    const validationSchema = getValidationSchema(values)
    try {
      validationSchema.validateSync(values, { abortEarly: false })
      return {}
    } catch (error) {
      return getErrorsFromValidationError(error)
    }
  }
}

const getErrorsFromValidationError = (validationError) => {
  const FIRST_ERROR = 0
  return validationError.inner.reduce((errors, error) => {
    return {
      ...errors,
      [error.path]: error.errors[FIRST_ERROR],
    }
  }, {})
}

const initialValues = {
  dependentLicenseNumber:"",
  dependentNationalId: "",
  relationshipToBeneficiary: "",
  dependentFirstName: "",
  dependentLastName: "",
  dependentEmailAddress: "",
  dependentDateOfBirth: "",
  dependentPhoneNumber: "",
  dependentGender:"",

}

const onSubmit = (values, { setSubmitting }) => {
  setTimeout(() => {
    alert(JSON.stringify(values, null, 2))
     console.log('User has been successfully saved!', values)
    setSubmitting(false)
  }, 2000)
}



const OtherUsersDetails = () =>{




    /*
     const  findFirstError =(formName, hasError) => {
        const form = document.forms[formName]
        for (let i = 0; i < form.length; i++) {
          if (hasError(form[i].name)) {
            form[i].focus()
            break
          }
        }
      }

     const  validateForm = (errors)=> {
        findFirstError('simpleForm', (fieldName) => {
       return Boolean(errors[fieldName])
        })
      }

    /*
     const  touchAll = (setTouched, errors) =>{
        setTouched({
            dependentLicenseNumber:true,
            dependentNationalId:true,
            relationshipToBeneficiary:true,
            dependentFirstName:true,
            dependentLastName:true,
            dependentEmailAddress:true,
            dependentDateOfBirth:true,
            dependentPhoneNumber:true,
            dependentGender:true,

          }
        )
        validateForm(errors)
      }*/
  return(
  
    <div className="animated fadeIn">
    <Link to="/new/type" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
    
      <Card>
        <CardHeader>
        <strong>Car Insurance Application Form</strong>
        </CardHeader>
        <CardBody>
          <strong>Secondary Drivers' Information</strong>
          <hr/>
          <Formik
            initialValues={initialValues}
            validate={validate(validationSchema)}
            onSubmit={onSubmit}
            render={
              ({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit
              }) => (
                
                    <Form onSubmit={handleSubmit} noValidate name='simpleForm'>
                    
                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                      <FormGroup>
                        <Label for="dependentNationalId">National Id/ Passport Number</Label>
                        <Input type="text"
                              name="dependentNationalId"
                              id="dependentNationalId"
                              placeholder="National Id/ Passport Number"
                              autoComplete="national-id"
                              valid={!errors.dependentNationalId}
                              invalid={touched.dependentNationalId && !!errors.dependentNationalId}
                              autoFocus={true}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.dependentNationalId} />
                      <FormFeedback>{errors.dependentNationalId}</FormFeedback>
                      </FormGroup>
                      </Col>
                      <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="relationshipToBeneficiary">Relationship To Beneficiary</Label>
                        <Input type="select" name="relationshipToBeneficiary" id="relationshipToBeneficiary"
                        valid={!errors.relationshipToBeneficiary}
                        invalid={touched.relationshipToBeneficiary && !!errors.relationshipToBeneficiary}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.relationshipToBeneficiary} >
                          <option value="" disabled selected hidden>Select</option> 
                          <option value="">Other</option>
                          <option value="husband">Husband</option>
                          <option value="wife">Wife</option>
                          <option value="son">Son</option>
                          <option value="daughter">Daughter</option>
                          <option value="sister">Sister</option>
                          <option value="brother">Brother</option>
                          <option value="friend">Friend</option>
                        </Input>
                        <FormFeedback>{errors.relationshipToBeneficiary}</FormFeedback>
                      </FormGroup>
                    </Col>
                      </FormGroup>

                      
                    <FormGroup  row className="my-0">
                      <Col  xs="12" sm="6"  lg="6">
                        <FormGroup>
                          <Label for="dependentFirstName">First Name</Label>
                          <Input type="text"
                                name="dependentFirstName"
                                id="dependentFirstName"
                                placeholder="First Name"
                                autoComplete="first-name"
                                valid={!errors.dependentFirstName}
                                invalid={touched.dependentFirstName && !!errors.dependentFirstName}
                                required
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.dependentFirstName} />
                          <FormFeedback>{errors.dependentFirstName}</FormFeedback>
                        </FormGroup>
                        </Col>

                      <Col  xs="12" sm="6"  lg="6">
                      <FormGroup>
                        <Label for="dependentLastName">Last Name</Label>
                        <Input type="text"
                              name="dependentLastName"
                              id="dependentLastName"
                              placeholder="Last Name"
                              autoComplete="username"
                              valid={!errors.dependentLastName}
                              invalid={touched.dependentLastName && !!errors.dependentLastName}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.dependentLastName} />
                        <FormFeedback>{errors.dependentLastName}</FormFeedback>
                      </FormGroup>
                      </Col>
                    </FormGroup>


                    <FormGroup  row className="my-0">
                      <Col  xs="12" sm="6"  lg="6">
                        <FormGroup>
                        <Label for="dependentEmail">Email</Label>
                        <Input type="dependentEmail"
                              name="dependentEmailAddress"
                              id="dependentEmailAddress"
                              placeholder="Email"
                              autoComplete="email"
                              valid={!errors.dependentEmailAddress}
                              invalid={touched.dependentEmailAddress && !!errors.dependentEmailAddress}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.dependentEmailAddress} />
                        <FormFeedback>{errors.dependentEmailAddress}</FormFeedback>
                      </FormGroup>
                      </Col>

                      <Col  xs="12" sm="6"  lg="6">
                          <FormGroup>
                            <Label for="dependentPhoneNumber">Phone Number</Label>
                            <Input type="text"
                                  name="dependentPhoneNumber"
                                  id="dependentPhoneNumber"
                                  placeholder="Phone Number"
                                  autoComplete="phone-number"
                                  valid={!errors.dependentPhoneNumber}
                                  invalid={touched.dependentPhoneNumber && !!errors.dependentPhoneNumber}
                                  required
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.dependentPhoneNumber} />
                            <FormFeedback>{errors.dependentPhoneNumber}</FormFeedback>
                          </FormGroup>
                        </Col>
                    </FormGroup>
                    

                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                          <FormGroup>
                            <Label for="dependentDateOfBirth">Date of Birth</Label>
                            <Input type="date"
                                  name="dependentDateOfBirth"
                                  id="dependentDateOfBirth"
                                  placeholder="yyyy/mm/dd"
                                  autoComplete="new-dateOfBirth"
                                  valid={!errors.dependentDateOfBirth}
                                  invalid={touched.dependentDateOfBirth && !!errors.dependentDateOfBirth}
                                  required
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.dependentDateOfBirth} />
                            <FormFeedback>{errors.dependentDateOfBirth}</FormFeedback>
                          </FormGroup>
                        </Col>
                      
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="dependentGender">Gender</Label>
                        <Input type="select" name="dependentGender" id="dependentGender"
                        valid={!errors.dependentGender}
                        invalid={touched.dependentGender && !!errors.dependentGender}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.dependentGender} >
                          <option value="" disabled selected hidden>Select</option> 
                          <option value="">Other</option>
                          <option value="male">Male</option>
                          <option value="female">Female</option>
                        </Input>
                        <FormFeedback>{errors.dependentGender}</FormFeedback>
                      </FormGroup>
                    </Col>
                    </FormGroup>


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                      <FormGroup>
                        <Label for="dependentLicenseNumber">License Number</Label>
                        <Input type="text"
                              name="dependentLicenseNumber"
                              id="dependentLicenseNumber"
                              placeholder="License Number"
                              autoComplete="dependent-license-number"
                              valid={!errors.dependentLicenseNumber}
                              invalid={touched.dependentLicenseNumber && !!errors.dependentLicenseNumber}
                              autoFocus={false}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.dependentLicenseNumber} />
                      <FormFeedback>{errors.dependentLicenseNumber}</FormFeedback>
                      </FormGroup>
                      </Col>
                      <Col  xs="12" sm="6"  lg="6">
                      <FormGroup>
                        <Label for="dateWhenLicenseWasObtained">When Did You Obtain Your License?</Label>
                        <Input type="date"
                              name="dateWhenLicenseWasObtained"
                              id="dateWhenLicenseWasObtained"
                              placeholder="yyyy/mm/dd"
                              autoComplete="dateWhenLicenseWasObtained"
                              valid={!errors.dateWhenLicenseWasObtained}
                              invalid={touched.dateWhenLicenseWasObtained && !!errors.dateWhenLicenseWasObtained}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.dateWhenLicenseWasObtained} />
                        <FormFeedback>{errors.dateWhenLicenseWasObtained}</FormFeedback>
                      </FormGroup>
                    </Col>
                    </FormGroup>
                    
                    </Form>

                
              )} />
        </CardBody>
      </Card>
    
    

  </div>
  )
}

export default OtherUsersDetails;
