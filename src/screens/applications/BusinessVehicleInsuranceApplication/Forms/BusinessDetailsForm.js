import {Link} from "react-router-dom";
import {Card, CardBody, CardHeader, Col, FormGroup, Label} from "reactstrap";
import React from "react";
import {InputField, InputSelect} from "../../FormFields";



export  const  BusinessDetailsForm = (props)=> {
  const {
    formField: {
    CompanyDetails: {
    companyDetails_clientCompanyClass,
    companyDetails_clientCompanyName,
    companyDetails_clientCIN,
    companyDetails_clientRegistrationNumber,
    companyDetails_clientBusinessDescription,
    }
    }

  } = props;


  const companyTypes = [
    {text: "Select your Company Type", value:""},
      {text: "Corporation", value: "corporation"},
      {text: "Cooperative", value: "cooperative"},
      {text: "Private", value: "private"},
      {text: "Nonprofit", value: "nonprofit"},
      {text: "Partnership", value: "partnership"},
  ]


   return(

    <div className="animated fadeIn">

          <strong>Company Details</strong>
          <hr />

                  <FormGroup  row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                         <InputField type="text"
                               label = {<Label htmlFor="companyDetails_clientCompanyName"><span style={{color:"red", size:12}}>*</span>Company Name</Label>}
                               name={companyDetails_clientCompanyName.name}
                               id={companyDetails_clientCompanyName.name}
                               placeholder="Company Name"/>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                     <InputSelect data={companyTypes}
                                  label = {<Label htmlFor="companyDetails_clientCompanyClass"><span style={{color:"red", size:12}}>*</span>Type of Company</Label>}
                                  name={companyDetails_clientCompanyClass.name}
                                  id={companyDetails_clientCompanyClass.name}
                                  value="value" text="text"/>
                    </Col>
        </FormGroup>

                  <FormGroup  row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                         <InputField type="text"
                               label = {<Label htmlFor="companyDetails_clientRegistrationNumber"><span style={{color:"red", size:12}}>*</span>Company Registration Number</Label>}
                               name={companyDetails_clientRegistrationNumber.name}
                               id={companyDetails_clientRegistrationNumber.name}
                               placeholder="Company Registration Number"/>


                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                         <InputField type="text"
                               label = {<Label htmlFor="companyDetails_clientCIN"><span style={{color:"red", size:12}}>*</span>Cooperate Identification Number (CIN)</Label>}
                               name={companyDetails_clientCIN.name}
                               id={companyDetails_clientCIN.name}
                               placeholder="CIN"/>

                    </Col>

        </FormGroup>
<div><br /></div>
                  <FormGroup  row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                         <InputField type="text"
                               label = "Business Type"
                               rows={2}
                               name={companyDetails_clientBusinessDescription.name}
                               id={companyDetails_clientBusinessDescription.name}
                               placeholder="Business Description"/>
                    </Col>

        </FormGroup>
    </div>
  )

}
