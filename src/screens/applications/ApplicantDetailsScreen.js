import React from 'react';
import { Button, Card, CardHeader, CardBody, Col, Form, FormFeedback, FormGroup, Label, Input} from 'reactstrap';
import {Link} from 'react-router-dom'
import { Formik } from 'formik';

import * as Yup from 'yup'


const validationSchema = function (values) {
  return Yup.object().shape({
    nationalId: Yup.string()
    .matches(/^\(?([0-9]{2})\)?[-. ]?([0-9]{7})([A-Z]{1})[-. ]?([0-9]{2})$/, 'Please Enter A Valid ID Number')
    .min(14, `National ID has to be at least 14 characters`)
    .required('National ID is required'),
    clientFirstName: Yup.string()
    .min(2, `First Name has to be at least 2 characters`)
    .required('Last Name is required'),
    clientLastName: Yup.string()
    .min(2, `Last Name has to be at least 2 characters `)
    .required('Username is required'),
    clientEmailAddress: Yup.string()
    .matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
    .email('Invalid email address')
    .required('Email is required!'),
    dateOfBirth: Yup.string()
    .required('Date of Birth Is Required'),
    clientPhoneNumber: Yup.string()
    .matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
    .max(10, 'Phone number should be at least 10 characters')
    .required('Phone Number is required'),
    clientGender: Yup.string()
    .required('Gender is required!'),
    clientAddressLineOne: Yup.string()
    .required('Address Line One Required!'),
    clientAddressLineTwo: Yup.string()
    .required('Address Line Two Required!'),
    clientRegion: Yup.string()
    .required('Region is Required!'),
    clientZip: Yup.string()
    .required('Zip is Required!'),
    clientCountry: Yup.string()
    .required('Client Country is Required!')
  })
}

const validate = (getValidationSchema) => {
  return (values) => {
    const validationSchema = getValidationSchema(values)
    try {
      validationSchema.validateSync(values, { abortEarly: false })
      return {}
    } catch (error) {
      return getErrorsFromValidationError(error)
    }
  }
}

const getErrorsFromValidationError = (validationError) => {
  const FIRST_ERROR = 0
  return validationError.inner.reduce((errors, error) => {
    return {
      ...errors,
      [error.path]: error.errors[FIRST_ERROR],
    }
  }, {})
}

const initialValues = {
  nationalId: "",
  clientFirstName: "",
  clientLastName: "",
  clientEmailAddress: "",
  dateOfBirth: "",
  clientPhoneNumber: "",
  clientGender:"",
  clientAddressLineOne:"",
  clientAddressLineTwo:"",
  clientRegion:"",
  clientZip:"",
  clientCountry:""
  
}

const onSubmit = (values, { setSubmitting, setErrors }) => {
  setTimeout(() => {
    alert(JSON.stringify(values, null, 2))
     console.log('User has been successfully saved!', values)
    setSubmitting(false)
  }, 2000)
}



const ApplicantDetailsScreen = () =>{


  

 const  findFirstError =(formName, hasError) => {
    const form = document.forms[formName]
    for (let i = 0; i < form.length; i++) {
      if (hasError(form[i].name)) {
        form[i].focus()
        break
      }
    }
  }

 const  validateForm = (errors)=> {
    findFirstError('simpleForm', (fieldName) => {
      return Boolean(errors[fieldName])
    })
  }


 const  touchAll = (setTouched, errors) =>{
    setTouched({
        nationalId: true,
        clientFirstName: true,
        clientLastName: true,
        clientEmailAddress: true,
        dateOfBirth: true,
        clientPhoneNumber: true,
        clientGender: true,
        clientAddressLineOne:true,
        clientAddressLineTwo:true,
        clientRegion:true,
        clientZip:true,
        clientCountry:true
      
      }
    )
    validateForm(errors)
  }
  return(
    <div className="animated fadeIn">
    <Link to="/new/type" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
    <Card>
      <CardHeader>
       <strong>Applicant Details</strong>
      </CardHeader>
      <CardBody>
        <strong>Personal Details</strong>
        <hr />
        <Formik
          initialValues={initialValues}
          validate={validate(validationSchema)}
          onSubmit={onSubmit}
          render={
            ({
              values,
              errors,
              touched,
              status,
              dirty,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              isValid,
              handleReset,
              setTouched
            }) => (
              <>
                  <Form onSubmit={handleSubmit} noValidate name='simpleForm'>
                    <FormGroup row className="my-0">
                    <Col xs={12}>
                    <FormGroup>
                      <Label for="nationalId">National Id</Label>
                      <Input type="text"
                             name="nationalId"
                             id="nationalId"
                             placeholder="National Id"
                             autoComplete="national-id"
                             valid={!errors.nationalId}
                             invalid={touched.nationalId && !!errors.nationalId}
                             autoFocus={true}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.nationalId} />
                    <FormFeedback>{errors.nationalId}</FormFeedback>
                    </FormGroup>
                    </Col>
                    </FormGroup>

                  <FormGroup  row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                      <FormGroup>
                        <Label for="clientFirstName">First Name</Label>
                        <Input type="text"
                              name="clientFirstName"
                              id="clientFirstName"
                              placeholder="First Name"
                              autoComplete="first-name"
                              valid={!errors.clientFirstName}
                              invalid={touched.clientFirstName && !!errors.clientFirstName}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.clientFirstName} />
                        <FormFeedback>{errors.clientFirstName}</FormFeedback>
                      </FormGroup>
                      </Col>

                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                      <Label for="clientLastName">Last Name</Label>
                      <Input type="text"
                             name="clientLastName"
                             id="clientLastName"
                             placeholder="User Name"
                             autoComplete="username"
                             valid={!errors.clientLastName}
                             invalid={touched.clientLastName && !!errors.clientLastName}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.clientLastName} />
                      <FormFeedback>{errors.clientLastName}</FormFeedback>
                    </FormGroup>
                    </Col>
                  </FormGroup>

                  <FormGroup  row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                      <FormGroup>
                      <Label for="email">Email</Label>
                      <Input type="email"
                             name="clientEmailAddress"
                             id="clientEmailAddress"
                             placeholder="Email"
                             autoComplete="email"
                             valid={!errors.clientEmailAddress}
                             invalid={touched.clientEmailAddress && !!errors.clientEmailAddress}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.clientEmailAddress} />
                      <FormFeedback>{errors.clientEmailAddress}</FormFeedback>
                    </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                        <FormGroup>
                          <Label for="clientPhoneNumber">Phone Number</Label>
                          <Input type="text"
                                 name="clientPhoneNumber"
                                 id="clientPhoneNumber"
                                 placeholder="Phone Number"
                                 autoComplete="phone-number"
                                 valid={!errors.clientPhoneNumber}
                                 invalid={touched.clientPhoneNumber && !!errors.clientPhoneNumber}
                                 required
                                 onChange={handleChange}
                                 onBlur={handleBlur}
                                 value={values.clientPhoneNumber} />
                          <FormFeedback>{errors.clientPhoneNumber}</FormFeedback>
                        </FormGroup>
                      </Col>
                  </FormGroup>
                  


                  <FormGroup row className="my-0">
                  <Col  xs="12" sm="6"  lg="6">
                        <FormGroup>
                          <Label for="dateOfBirth">Date of Birth</Label>
                          <Input type="date"
                                 name="dateOfBirth"
                                 id="dateOfBirth"
                                 placeholder="Password"
                                 autoComplete="new-dateOfBirth"
                                 valid={!errors.dateOfBirth}
                                 invalid={touched.dateOfBirth && !!errors.dateOfBirth}
                                 required
                                 onChange={handleChange}
                                 onBlur={handleBlur}
                                 value={values.dateOfBirth} />
                          <FormFeedback>{errors.dateOfBirth}</FormFeedback>
                        </FormGroup>
                      </Col>
                     
                  <Col  xs="12" sm="6"  lg="6">
                  <FormGroup>
                      <Label htmlFor="clientGender">Gender</Label>
                      <Input type="select" name="clientGender" id="clientGender"
                       valid={!errors.clientGender}
                       invalid={touched.clientGender && !!errors.clientGender}
                       required
                       onChange={handleChange}
                       onBlur={handleBlur}
                       value={values.clientGender} >
                        <option value="">Other</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                      </Input>
                      <FormFeedback>{errors.clientGender}</FormFeedback>
                    </FormGroup>
                  </Col>
                  </FormGroup>
                    
                  <strong>Address Information</strong>
                  <hr/>

                  <FormGroup row className="my-0">
                    <Col xs={12}>
                    <FormGroup>
                    <Label for="clientAddressLineOne">Address Line One</Label>
                      <Input type="textarea"
                             name="clientAddressLineOne"
                             id="clientAddressLineOne"
                             placeholder="Address Line 1"
                             autoComplete="clientAddressLineOne"
                             valid={!errors.clientAddressLineOne}
                             invalid={touched.clientAddressLineOne && !!errors.clientAddressLineOne}
                             autoFocus={false}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.clientAddressLineOne} />
                    <FormFeedback>{errors.clientAddressLineOne}</FormFeedback>
                    </FormGroup>
                    </Col>
                  </FormGroup>


                  <FormGroup row className="my-0" >
                  <Col xs={12}>
                    <FormGroup>
                    <Label for="clientAddressLineOne">Address Line Two</Label>
                      <Input type="textarea"
                             name="clientAddressLineTwo"
                             id="clientAddressLineTwo"
                             placeholder="Address Line 2"
                             autoComplete="clientAddressLineTwo"
                             valid={!errors.clientAddressLineTwo}
                             invalid={touched.clientAddressLineTwo && !!errors.clientAddressLineTwo}
                             autoFocus={false}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.clientAddressLineTwo} />
                    <FormFeedback>{errors.clientAddressLineTwo}</FormFeedback>
                    </FormGroup>
                    </Col>
                  </FormGroup>



                  <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                    <Label for="clientRegion">Region</Label>
                      <Input type="text"
                             name="clientRegion"
                             id="clientRegion"
                             placeholder="Region / City / State"
                             autoComplete="clientRegion"
                             valid={!errors.clientRegion}
                             invalid={touched.clientRegion && !!errors.clientRegion}
                             autoFocus={false}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.clientRegion} />
                    <FormFeedback>{errors.clientRegion}</FormFeedback>

                    </FormGroup>
                    </Col>
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                    <Label for="clientZip">Zip Code</Label>
                      <Input type="text"
                             name="clientZip"
                             id="clientZip"
                             placeholder="Enter Zip Code"
                             autoComplete="clientZip"
                             valid={!errors.clientZip}
                             invalid={touched.clientZip && !!errors.clientZip}
                             autoFocus={false}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.clientZip} />
                    <FormFeedback>{errors.clientZip}</FormFeedback>
                    </FormGroup>
                    </Col>
                  </FormGroup>

                  <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                    <Label for="clientCountry">Country</Label>
                      <Input type="text"
                             name="clientCountry"
                             id="clientCountry"
                             placeholder="Country"
                             autoComplete="clientCountry"
                             valid={!errors.clientCountry}
                             invalid={touched.clientCountry && !!errors.clientCountry}
                             autoFocus={false}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.clientCountry} />
                    <FormFeedback>{errors.clientCountry}</FormFeedback>

                    </FormGroup>
                    </Col>
                  </FormGroup>

                    <FormGroup>
                      <Button type="submit" color="success" className="mr-1" disabled={isSubmitting || !isValid}>{isSubmitting ? 'Wait...' : 'Submit Application'}</Button>
                    </FormGroup>
                  </Form>
              


               
              </>
            )} />
      </CardBody>
    </Card>
  </div>
  )
}






export default ApplicantDetailsScreen;
