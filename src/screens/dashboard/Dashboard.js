import React from "react";
import { Col, Row } from "reactstrap";
import DashBoardCards from "../../components/cards/DashBoardCards";
import useTrans from "../../hooks/useTrans";

const Dashboard = () => {
  // eslint-disable-next-line
  const [t, handleClick] = useTrans();

  return (
    <div className="animated fadeIn">
      <Row>
        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Claims")}
            footerText={t("ManageClaims")}
            cardRoute = "claims"
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Premium")}
            footerText={t("ManagePremiums")}
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Quotation")}
            footerText={t("ManageQuotations")}
            cardRoute="quotation"
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Rewards")}
            footerText={t("ManageRewards")}
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Underwriting")}
            footerText={t("ManageUnderwriting")}
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Service")}
            footerText={t("ManageService")}
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Commissions")}
            footerText={t("ManageCommission")}
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
        <DashBoardCards
          cardTitle={t("ServiceProviders")}
          footerText={t("Providers")}
          cardRoute="find"
        />
      </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Register")}
            footerText={t("Regis")}
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={"New Business Application"}
            footerText={t("Apply")}
            cardRoute="new"
          />
        </Col>
      </Row>
    </div>
  );
};

export default Dashboard;