import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

export const onSuccess = () => {
  const MySwal = withReactContent(Swal);
  MySwal.fire("Claim Submitted!", "Thank you!", "success");
};

export const onError = () => {
  const MySwal = withReactContent(Swal);
  MySwal.fire("Error Submitting Claim!", "Please Try Again!", "error");
};
export const onSuccessAssesor = () => {
  const MySwal = withReactContent(Swal);
  MySwal.fire("Assesor Assigned Successfully!", "Thank You!", "success");
};

export const onErrorAssesor = () => {
  const MySwal = withReactContent(Swal);
  MySwal.fire("Error Assigning Assesor!", "Please Try Again!", "error");
};


export const onSuccessMoreDetails = () => {
  const MySwal = withReactContent(Swal);
  MySwal.fire("Getting More Details!", "Thank You!", "success");
};

export const onErrorMoreDails = () => {
  const MySwal = withReactContent(Swal);
  MySwal.fire("Error Getting More Details!", "Please Try Again!", "error");
};