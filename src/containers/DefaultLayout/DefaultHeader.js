import React, { useState } from 'react'
import { Nav, NavItem, NavLink, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { AppAsideToggler, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import DefaultHeaderDropdown from './DefaultHeaderDropdown'
import logo from '../../assets/img/brand/logo.png'
import sygnet from '../../assets/img/brand/sygnet.svg'
import useTrans from '../../hooks/useTrans';
import useKeyCloak from '../../hooks/useKey';
import { useKeycloak } from '@react-keycloak/web'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

const DefaultHeader = (props) => {
  
  // eslint-disable-next-line
  const [t, handleClick] = useTrans();
  // eslint-disable-next-line
  const { keycloak, initialized } = useKeycloak()

  // localStorage.setItem('kc_refreshToken', keycloak.refreshToken)
  // localStorage.setItem('kc_token', keycloak.token)

  // console.log(keycloak.token)




  // eslint-disable-next-line
  const { children, ...attributes } = props;

  const [dropdownOpen, setDropdownOpen] = useState(false);
  const toggle = () => setDropdownOpen(prevState => !prevState);



  return (
    <React.Fragment>
      <AppSidebarToggler className="d-lg-none" display="md" mobile />
      <AppNavbarBrand full={{ src: logo, width: 100, height: 50, alt: 'InsureHub' }}
        minimized={{ src: sygnet, width: 30, height: 30, alt: 'InsureHub' }} />
      <AppSidebarToggler className="d-md-down-none" display="lg" />
      <Nav className="d-md-down-none" navbar>
        <NavItem className="px-3">
          <NavLink href="/claims">{t("Claims.1")}</NavLink>
        </NavItem>
        <NavItem className="px-3">
          <NavLink to="#">{t("Premium.1")}</NavLink>
        </NavItem>
        <NavItem className="px-3">
          <NavLink href="#">{t("Quotation.1")}</NavLink>
        </NavItem>
        <NavItem className="px-3">
          <NavLink href="#">{t("Rewards.1")}</NavLink>
        </NavItem>
        <NavItem className="px-3">
          <NavLink href="#">{t("Underwriting.1")}</NavLink>
        </NavItem>
        <NavItem className="px-3">
          <NavLink href="#">{t("Service.1")}</NavLink>
        </NavItem>
        <NavItem className="px-3">
          <NavLink href="#">{t("Commissions.1")}</NavLink>
        </NavItem>
      </Nav>
      <Nav className="ml-auto" navbar>
        {/* <DefaultHeaderDropdown onLogout={props.onLogout} accnt /> */}
        <NavItem>
          <Dropdown isOpen={dropdownOpen} toggle={toggle}  >
            <DropdownToggle className="nav-link">
              <NavLink>
                <span className="material-icons">translate arrow_drop_down</span>
              </NavLink>
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem value="en" onClick={() => handleClick("en")}>EN</DropdownItem>
              <DropdownItem value="ko" onClick={() => handleClick("ko")}>KO</DropdownItem>
              <DropdownItem value="chi" onClick={() => handleClick("chi")}>CHI</DropdownItem>
              <DropdownItem value="sho" onClick={() => handleClick("sho")}>SH</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </NavItem>
      </Nav>
      <AppAsideToggler className="d-md-down-none" />
      <AppAsideToggler className="d-lg-none" mobile />


        
        {keycloak.authenticated && (
            <Button type="button" color="danger" onClick={() => keycloak.logout()}>
              Logout( {keycloak.tokenParsed.preferred_username})
            </Button>
            )
         }


    </React.Fragment>
  )
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
