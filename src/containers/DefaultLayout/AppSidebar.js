import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import {
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarToggler,
  CCreateNavItem,
} from '@coreui/react'
import logo from '../../assets/img/brand/logo.png'
import CIcon from '@coreui/icons-react'

import SimpleBar from 'simplebar-react'
import 'simplebar/dist/simplebar.min.css'

// sidebar nav config
import navigation from '../../_nav'
import {NavLink} from "react-router-dom";
import {layoutSideBarUnfoldable, loadLayout} from "../../store/actions";

const AppSidebar = () => {
  const dispatch = useDispatch()
  const unfoldable = useSelector((state) => state.layout?.sidebarUnfoldable)
  const sidebarShow = useSelector((state) => state.layout?.sidebarShow)

  return (
    <CSidebar
      position="fixed"
      selfHiding="md"
      unfoldable={unfoldable}
      show={sidebarShow}
      onShow={() => console.log('show')}
      onHide={() => {
        //call action
        dispatch(layoutSideBarUnfoldable(false))
      }}
    >
      <CSidebarBrand className="d-none d-md-flex" to="/dashboard" component={NavLink} >
        <CIcon className="sidebar-brand-full" name="logo-negative" src={logo}  height={100} />
        <CIcon className="sidebar-brand-narrow" name="sygnet" src={logo} height={38} />
      </CSidebarBrand>
      <CSidebarNav>
        <SimpleBar>
          <CCreateNavItem items={navigation} />
        </SimpleBar>
      </CSidebarNav>
      <CSidebarToggler
        className="d-none d-lg-flex"
        onClick={() => dispatch(layoutSideBarUnfoldable(!unfoldable ))}
      />
    </CSidebar>
  )
}

export default React.memo(AppSidebar)
