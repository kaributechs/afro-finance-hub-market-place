import AxiosInstance from './axios-interceptor';

export class HttpService {

  getHttp() {
    return AxiosInstance;
  }


  delete(url, data) {
    return this.getHttp().delete(url, { data: data })
      .then(res => res.data)
  }

  get(url) {
    return this.getHttp().get(url)
      .then(res => res.data);
  }

  post(url, data) {
    return this.getHttp().post(url, data)
      .then(res => res.data)
  }

  put(url, data) {
    return this.getHttp().put(url, data)
      .then(res => res.data);
  }

  uploadFile(url, formData){
    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    };
    return  this.getHttp().post(url, formData,config)
  }





}

