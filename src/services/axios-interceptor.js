import axios from 'axios';

const TIMEOUT = 1 * 60 * 1000;
axios.defaults.timeout = TIMEOUT;
axios.defaults.baseURL = "";

let axiosInstance = axios.create();
//Axios Request Interceptors => Before Request

const onResponseSuccess = response => response;
const onResponseError = err => {
  const status = err.status || (err.response ? err.response.status : 0);
  if (status === 401) {
    //redirect to login page

  }else if(status === 403){
    //redirect to not-authorised page
  }
  return Promise.reject(err.response?.data);
};
axiosInstance.interceptors.request.use((request)=>{
  console.log('**Request**',request)
  request.headers.Authorization =  `Bearer ${localStorage.getItem('kc_token')}`;
  return request;
});
axiosInstance.interceptors.response.use(onResponseSuccess, onResponseError);

export default axiosInstance;

