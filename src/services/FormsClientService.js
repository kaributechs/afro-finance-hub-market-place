import {HttpService} from "./HttpService";

export class FormsClientService extends HttpService {
//Get form data
  fetchFormData  (event) {
    return this.get(`/api/v1/${event.id}/get`);
  }

//Save form data 
  sendFormData (event)  {
    return this.post(`/api/v1/${event.id}/save`);
  }


}
